package service;

import bean.AreaCode;
import util.DbUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AreaCodeService {
    public AreaCode getAreaCode(Integer id){
        Connection connection=null;
        PreparedStatement preparedStatement=null;
        ResultSet resultSet=null;
        AreaCode areaCode=new AreaCode();
        try {
            connection= DbUtil.getCon();
            connection.setAutoCommit(false);
            String sql="SELECT * FROM PUBLIC.quhua WHERE id=?";
            preparedStatement=connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            resultSet=preparedStatement.executeQuery();
            while (resultSet.next()){
            areaCode.setProvince(resultSet.getString(2));
            areaCode.setCity(resultSet.getString("city"));
            areaCode.setDistrict(resultSet.getString("district"));
            areaCode.setCountry(resultSet.getString("country"));
            areaCode.setCommitee(resultSet.getString("commitee"));
            areaCode.setAreacode(resultSet.getString("areacode"));
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();



        } catch (SQLException e) {
            e.printStackTrace();
        }
      return areaCode;

    }

    public static void main(String[] args) {
        AreaCodeService areaCodeService=new AreaCodeService();
        System.out.println(areaCodeService.getAreaCode(1000).getAreacode());
    }

}
