package service;

import bean.Worker;
import util.DbUtil;

import java.sql.*;

public class WorkerService {
    public void insert(Worker worker){
        Connection connection=null;
        PreparedStatement preparedStatement=null;

        try {
            connection= DbUtil.getCon();
            connection.setAutoCommit(false);
            String sql="INSERT  INTO  worker(name,farmName,birthdate,crops,tel,company,idnum) VALUES(?,?,?,?,?,?,?)";
            preparedStatement=connection.prepareStatement(sql);
            Date date=new Date(worker.getBirthdate().getTime());
            preparedStatement.setString(1,worker.getName());
            preparedStatement.setString(2,worker.getFarmname());
            preparedStatement.setDate(3,date);
            preparedStatement.setString(4,worker.getCrops());
            preparedStatement.setString(5,worker.getTel());
            preparedStatement.setString(6,worker.getCompany());
            preparedStatement.setString(7,worker.getIdnum());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.commit();
            connection.close();

            } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
