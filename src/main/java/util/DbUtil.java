package util;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbUtil {
   public static String driver="org.postgresql.Driver";
   public static String url="jdbc:postgresql://localhost:5432/postgis_test";
   public static String username="postgres";
   public static String password="123456";

   public static Connection getCon(){
      Connection c=null;
      try {
         Class.forName(driver);
         c= DriverManager.getConnection(url,username,password);
      } catch (Exception e) {
         System.out.println("未找到类");
         e.printStackTrace();
      }
      return c;
   }
}
