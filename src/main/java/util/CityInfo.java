package util;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CityInfo {
    private static final String Local_List_Path="src\\main\\resources\\static\\LocList.xml";
    private static final List<String> Country_List=new ArrayList<String>();
    private SAXReader reader;
    private Document document;
    private Element rootElement;
    //初始化
    public CityInfo(){
        reader=new SAXReader();
        try {
            document=reader.read(Local_List_Path);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        rootElement=document.getRootElement();
        Iterator iterator=rootElement.elementIterator();
        Element e=null;
        while (iterator.hasNext()){
            e= (Element) iterator.next();
            Country_List.add(e.attributeValue("Name"));
        }
    }
    //获取所有的国家列表
    public List<String> getCountry(){
        return Country_List;
    }
    //获取所有省份的element
    public List<Element> getProvinceElements(String CountryName){
        Iterator iterator=rootElement.elementIterator();
        List<Element> provinces=new ArrayList<Element>();

        Element e=null;
        while (iterator.hasNext()){
            e= (Element) iterator.next();
            if(e.attributeValue("Name").equals(CountryName)){
                provinces=e.elements();
                break;
            }
        }
        return provinces;
    }
    //根据国家获取该国所有的省份
    public List<String> getProvince(String CountryName){
       List<Element> provinces=this.getProvinceElements(CountryName);
        List<String> list=new ArrayList<String>();
        for(int i=0;i<provinces.size();i++){
            list.add(provinces.get(i).attributeValue("Name"));
        }
        return list;
    }
    //获取城市的element
    private List<Element> getCitiesElement(String CountryName,String province){
        List<Element> cities=new ArrayList<Element>();
        List<Element> provinces=this.getProvinceElements(CountryName);
        for (int i=0;i<provinces.size();i++){
            if (provinces.get(i).attributeValue("Name").equals(province)){
                cities=provinces.get(i).elements();
                break;
            }
        }
        return cities;

    }
    //根据国家和省份获取所有的城市
    public List<String> getCities(String CountryName,String province){
        List<Element> cities=this.getCitiesElement(CountryName,province);
        List<String> list=new ArrayList<String>();
        for (int i=0;i<cities.size();i++){
            list.add(cities.get(i).attributeValue("Name"));
        }
        return list;
    }

    public List<String> getLocale(String CountryName,String province,String city){
        List<Element> cities=this.getCitiesElement(CountryName,province);
        List<Element> locales=new ArrayList<Element>();
        List<String>  list=new ArrayList<String>();
        for(int i=0;i<cities.size();i++){
            if(cities.get(i).attributeValue("Name").equals(city)){
                locales=cities.get(i).elements();
                break;} }
        for (int j=0;j<locales.size();j++){
            list.add(locales.get(j).attributeValue("Name"));
        }
        return list;
    }
    public static void main(String[] args) {
        CityInfo cityInfo=new CityInfo();
        List<String> list=cityInfo.getLocale("中国","广东","深圳");
        System.out.println(list.get(2));
    }
}
