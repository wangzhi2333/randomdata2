package util;

import service.AreaCodeService;

import java.util.Random;

public class RandomAreaCode {
    static AreaCodeService areaCodeService=new AreaCodeService();
    static Random random=new Random();
    //到村委会的区划代码 第五级
    public static String getRandomAreaCode(){
        return areaCodeService.getAreaCode(random.nextInt(666261)).getAreacode();
    }
    //到乡镇的区划代码 第四级
    public static String getRandimAreaCode2(){
        char code[]=areaCodeService.getAreaCode(random.nextInt(666261)).getAreacode().toCharArray();
        for(int i=9;i<=11;i++){
            code[i]='0';
        }
        return new String(code);
    }
    //到县级的区划代码 第三级
    public static String getRandimAreaCode3(){
        char code[]=areaCodeService.getAreaCode(random.nextInt(666261)).getAreacode().toCharArray();
        for(int i=6;i<=11;i++){
            code[i]='0';
        }
        return new String(code);
    }
    //到市一级的区划代码 第二级
    public static String getRandimAreaCode4(){
        char code[]=areaCodeService.getAreaCode(random.nextInt(666261)).getAreacode().toCharArray();
        for(int i=4;i<=11;i++){
            code[i]='0';
        }
        return new String(code);
    }
    //到省一级的区划代码 最高一级
    public static String getRandimAreaCode5(){
        char code[]=areaCodeService.getAreaCode(random.nextInt(666261)).getAreacode().toCharArray();
        for(int i=2;i<=11;i++){
            code[i]='0';
        }
        return new String(code);
    }

    public static void main(String[] args) {
        System.out.println(getRandomAreaCode());
        System.out.println(getRandimAreaCode2());
        System.out.println(getRandimAreaCode3());
        System.out.println(getRandimAreaCode4());
        System.out.println(getRandimAreaCode5());
    }
}
