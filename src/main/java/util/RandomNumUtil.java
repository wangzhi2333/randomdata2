package util;

import nl.flotsam.xeger.Xeger;

import java.util.Random;

public class RandomNumUtil {


    public static String getRandomCellPhone(){
        String regex="((13[0-9])|(14([5]|[7]|[9]))|(15([0-3]|[5-9]))|(166)|(17([0-1]|[3]|[5-8]))|(18[0-9])|(19[8-9]))[0-9]{8}";
        Xeger generator=new Xeger(regex);
        return getRandomData(regex);
    }
    public static String getRandomPhone(){
        Random r=new Random();
        String PhoneRe[]={"[0][0-9]{2,3}-[0-9]{5,10}","[0][0-9]{10,11}","[2-9]{1}[0-9]{6,7}"};
        String regex=PhoneRe[r.nextInt(3)];
        return getRandomData(regex);
    }
    //此方法不支持很复杂的表达式，也不支持特殊字符比如开头结尾的符号，同时还有不支持中括号里边的逗号，以及/d等都不支持
  public static String getRandomData(String regex){
        Xeger gen=new Xeger(regex);
        return gen.generate();
  }

    public static void main(String[] args) {
        for (int i=0;i<=10;i++){
            System.out.println(getRandomPhone());
            System.out.println(getRandomCellPhone());
        }
    }
}
