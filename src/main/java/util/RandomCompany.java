package util;

import java.util.List;
import java.util.Random;

public class RandomCompany {
private static final String suffix="有限公司";

public static String getRandomCompany(){
    Random r=new Random();
    String RandomCompany="";
    int s;
    CityInfo cityInfo=new CityInfo();
    List<String> pro=cityInfo.getProvince("中国");
    String province=pro.get(r.nextInt(pro.size()));
    List<String> cities=cityInfo.getCities("中国",province);
    String city=cities.get(r.nextInt(cities.size()));
//    List<String> locales=cityInfo.getLocale("中国",province,city);
//    String locale="";
//    try {
//        int a=locales.size();
//       s=r.nextInt(a);
//       System.out.println(a);
//       locale=locales.get(s).trim();
//    }
//    catch(IllegalArgumentException e){
//        locale="";
//
//        e.printStackTrace();
//    }

    String name=RandomCNCharUtil.get(4);
    RandomCompany=province+city+name+suffix;
    return RandomCompany;
}

    public static void main(String[] args) {
        System.out.println(getRandomCompany());
    }
}
