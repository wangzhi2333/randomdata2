package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RandomDateUtil {
    String format;
    Date startdate;
    Date enddate;

    public RandomDateUtil(String format,Date startdate,Date enddate){
        this.format=format;
        this.startdate=startdate;
        this.enddate=enddate;
    }
    public static Date getRandomDate(String startdate,String enddate) throws ParseException{
        
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        Date start=simpleDateFormat.parse(startdate);
        Date end=simpleDateFormat.parse(enddate);

        if (start.getTime()>end.getTime())
        {return null;}
        long date = random(start.getTime(),end.getTime());
        return new Date(date);
    }

    private static long random(long begin, long end) {
        long rtn = begin + (long)(Math.random() * (end - begin));
        if(rtn == begin || rtn == end){
            return random(begin,end);
        }
        return rtn;


    }

    public static void main(String[] args) {

        try {
            System.out.println(getRandomDate("1990-2-15","2011-10-22"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
