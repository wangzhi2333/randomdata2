package util;

import java.io.UnsupportedEncodingException;
import java.util.Random;

public class RandomCNCharUtil {

    public static String getRandomCNChar() {
        String str = null;
        int highPos, lowPos;
        Random random = new Random();
        highPos = (176 + Math.abs(random.nextInt(71)));//区码，0xA0打头，从第16区开始，即0xB0=11*16=176,16~55一级汉字，56~87二级汉字
        random=new Random();
        lowPos = 161 + Math.abs(random.nextInt(94));//位码，0xA0打头，范围第1~94列
        byte[] bArr = new byte[2];
        bArr[0] = (new Integer(highPos)).byteValue();
        bArr[1] = (new Integer(lowPos)).byteValue();
        try {
            str = new String(bArr, "GB2312");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        return str;
    }
    public static String get(int num){
        String str="";
        for(int i=1;i<=num;i++)
            str=str+getRandomCNChar();
        return str;

    }

}
