package controller;

import bean.Worker;
import service.WorkerService;
import util.*;

import java.util.Date;
import java.text.ParseException;

public class WorkerController {
public  static WorkerService workerService;
public static Worker generateWorker() throws ParseException {
    String []fixed={"猪","牛","羊"};
    Worker worker=new Worker();
    String name= RandomCNCharUtil.get(3);
    String FarmName=RandomCNCharUtil.get(5);
    String Crops= RandomFixedDataUtil.getRandomFixdeData(fixed);
    Date date= RandomDateUtil.getRandomDate("2017-12-11","2019-12-11");
    String tel= RandomNumUtil.getRandomCellPhone();
    worker.setName(name);
    worker.setFarmname(FarmName);
    worker.setBirthdate(date);
    worker.setCrops(Crops);
    worker.setTel(tel);
    worker.setIdnum(IdCardGenerator.generate());
    worker.setCompany(RandomCompany.getRandomCompany());
    return worker;
}

    public static void main(String[] args) throws ParseException {
    workerService=new WorkerService();
        for(int i=0;i<10;i++)
            workerService.insert(generateWorker());
    }
}
