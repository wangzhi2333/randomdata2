# RandomData2

可以随机生成的数据的说明：

（1）随机生成电话号码，固定电话号码

     固话：RandomNumUtil.getRandomPhone()
     
     手机：RandomNumUtil.getRandomCellPhone()
     
（2）随机生成身份证号

     IdCardGenerator.generate()
     
（3）随机生成指定长度范围的电子邮箱

     RandomEmailUtil.getRandomEmail(int lMin,int lMax)
     
（4）随机生成m-n位由数字和字母组成的用户名

     RandomEmailUtil.getRandomUserName(int m,int n)
     
（5）随机生成性别男或女

     RandomEmailUtil.getRandomSex()
     
（6）随机生成[x,y]之间的整数

     RandomEmailUtil.getNum(x,y)
     
（7）随机从给定的数组中选取一个数据

     RandomFixedDataUtil.getRandomFixdeData(String fixed[])
     
（8）随机从给定的日期范围内生成一个日期

     RandomDateUtil.getRandomDate(String startdate,String enddate)
     
     这两个字符串必须得是yyyy-mm-dd格式的
     
（9）随机生成要求长度的汉字

     RandomCNCharUtil.get(int num)num是生成汉字的个数
     
（10）随机生成到县一级的公司名称

     RandomCompany.getRandomCompany()公司名称是四位
     
（11）随机生成十二位五级区划代码（省，市，县，乡镇，居委会）

     RandomAreaCode.getRandomAreaCode() 第五级行政区划
     
     RandomAreaCode.getRandomAreaCode2()第四级行政区划
     
     RandomAreaCode.getRandomAreaCode3()第三级行政区划
     
     RandomAreaCode.getRandomAreaCode4()第二级行政区划
     
     RandomAreaCode.getRandomAreaCode5()第一级行政区划
     
（12）根据正则表达式生成随机数据（仅支持一小部分的正则规范）

      RandomNumUtil.getRandomData(String regex) regex传入一个正则表达式
      
      此方法不支持很复杂的表达式，也不支持特殊字符比如开头结尾的符号，同时还有不支持中括号里边的逗号，以及/d等都不支持，只是支持简单的数字或者字符说明
      
###########

静态文件说明：

LocList指的是从国家到乡镇的xml文件

areacode.csv是用Python爬取区划代码查询网的区划信息，因为直接读这个csv文件比较耗时，所以还是将这个文件导入数据库随机查询的比较好

lib说明

xeger.jar和automaton.jar都是用来支持那个通过正则生成符合要求字符串的

